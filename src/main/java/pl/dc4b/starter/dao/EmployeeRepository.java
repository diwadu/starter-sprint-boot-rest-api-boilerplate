package pl.dc4b.starter.dao;

import pl.dc4b.starter.entity.Employee;
import pl.dc4b.starter.entity.Grade;

import java.util.List;

public interface EmployeeRepository extends GenericRepository<Employee> {

//    default List<Employee> findAllWhereGradeIsRegular() {
//        return findByGrade(Grade.REGULAR);
//    }

    List<Employee> findByGrade(Grade grade);

}
