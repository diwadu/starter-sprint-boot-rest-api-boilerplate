package pl.dc4b.starter.dao;

import org.springframework.stereotype.Repository;
import pl.dc4b.starter.entity.Role;

@Repository
public interface RoleRepository extends GenericRepository<Role> {
}
