package pl.dc4b.starter.dao;

import org.springframework.stereotype.Repository;
import pl.dc4b.starter.entity.User;

import java.util.List;

@Repository
public interface UserRepository extends GenericRepository<User> {
    User findByUsername(String username);
    List<User> findByRoles_RoleName(String roleName);
}
