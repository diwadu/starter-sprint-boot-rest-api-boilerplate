package pl.dc4b.starter.web.exception;

import org.hibernate.ObjectNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.nio.file.AccessDeniedException;

@ControllerAdvice
public class GenericRestExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler({ AccessDeniedException.class })
    public ResponseEntity<Object> handleAccessDeniedException(Exception ex, WebRequest request) {

        return ResponseEntity.status(HttpStatus.FORBIDDEN).body("This is forbidden exception.");
    }

    @ExceptionHandler({ ObjectNotFoundException.class })
    public ResponseEntity<Object> handleObjectNotFoundException(Exception ex, WebRequest request) {

        return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Not found this entity.");
    }
}
