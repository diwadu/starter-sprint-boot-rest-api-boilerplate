package pl.dc4b.starter.web.dto;

import lombok.Data;

@Data
public abstract class BaseDTO {
	protected Long id;
}
