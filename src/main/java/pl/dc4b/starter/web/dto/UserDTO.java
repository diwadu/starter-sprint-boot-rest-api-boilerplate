package pl.dc4b.starter.web.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

@Data
public class UserDTO extends BaseDTO{

    private String username;

    @JsonIgnore
    private String password;

    private String firstName;

    private String lastName;
}
