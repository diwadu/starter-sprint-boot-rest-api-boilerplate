package pl.dc4b.starter.web.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import pl.dc4b.starter.entity.Grade;

@Data
public class EmployeeDTO extends BaseDTO {

    private String firstName;

    private String lastName;

    private Grade grade;

    private int salary;
}
