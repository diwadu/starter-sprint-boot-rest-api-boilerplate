package pl.dc4b.starter.web.controller;

import org.springframework.security.access.annotation.Secured;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pl.dc4b.starter.entity.User;
import pl.dc4b.starter.web.BaseController;
import pl.dc4b.starter.web.dto.UserDTO;

@RestController
@RequestMapping("api/users")
public class UserController extends BaseController<User, UserDTO> {
}
