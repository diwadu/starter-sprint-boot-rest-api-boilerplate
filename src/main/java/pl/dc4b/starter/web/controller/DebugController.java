package pl.dc4b.starter.web.controller;

import org.modelmapper.ModelMapper;
import org.modelmapper.TypeMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.cache.CacheProperties;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import pl.dc4b.starter.dao.RoleRepository;
import pl.dc4b.starter.dao.UserRepository;
import pl.dc4b.starter.entity.User;
import pl.dc4b.starter.service.UserService;
import pl.dc4b.starter.web.dto.UserDTO;

import java.lang.reflect.ParameterizedType;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("debug")
public class DebugController {
    @Value("${app.name}")
    String appName;

    @Autowired
    UserRepository userRepository;

    @Autowired
    RoleRepository roleRepository;

    @Autowired
    protected ModelMapper modelMapper;

    private TypeMap<User, UserDTO> typeMap;
    private Class<UserDTO> dtoClass = UserDTO.class;


    @Secured("permitAll")
    @RequestMapping(method = RequestMethod.GET, path = "/users/public")
    public ResponseEntity<List<User>> getAllUsersPublic() {
        System.out.println(appName);
        return new ResponseEntity<List<User>>(userRepository.findAll(), HttpStatus.OK);
    }

    @PreAuthorize("hasAuthority('ADMIN_USER') or hasAuthority('STANDARD_USER')")
    @RequestMapping(method = RequestMethod.GET, path = "/users/private")
    public ResponseEntity<List<User>> getAllUsersPrivate() {
        return new ResponseEntity<List<User>>(userRepository.findAll(), HttpStatus.OK);
    }

    @Secured("permitAll")
    @RequestMapping(method = RequestMethod.GET, path = "/users/public/by-role")
    public ResponseEntity<List<UserDTO>> getUsersByRoleName() {
        System.out.println(appName);
        List<User> usersByRole = userRepository.findByRoles_RoleName("ADMIN_USER");
        List<UserDTO> usersDtoByRole = usersByRole.stream().map(u -> entityToDto(u)).collect(Collectors.toList());
        return new ResponseEntity<List<UserDTO>>(usersDtoByRole, HttpStatus.OK);
    }

    private UserDTO entityToDto(User entity) {

        if (typeMap == null) {
            typeMap = modelMapper.createTypeMap(entity, this.dtoClass);
        }
        UserDTO dto = typeMap.map(entity);
        return dto;
    }
}
