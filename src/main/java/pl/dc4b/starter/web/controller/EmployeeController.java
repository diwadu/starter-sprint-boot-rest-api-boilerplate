package pl.dc4b.starter.web.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pl.dc4b.starter.entity.Employee;
import pl.dc4b.starter.web.BaseController;
import pl.dc4b.starter.web.dto.EmployeeDTO;

@RestController
@RequestMapping("api/employees")
public class EmployeeController extends BaseController<Employee, EmployeeDTO> {

}
