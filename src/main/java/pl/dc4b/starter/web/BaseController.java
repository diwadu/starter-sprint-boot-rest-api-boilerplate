package pl.dc4b.starter.web;

import com.google.common.base.Joiner;
import org.hibernate.ObjectNotFoundException;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.dc4b.starter.web.dto.BaseDTO;
import pl.dc4b.starter.web.search.GenericSpecificationBuilder;
import pl.dc4b.starter.entity.BaseEntity;
import pl.dc4b.starter.web.search.SearchOperation;

import java.lang.reflect.ParameterizedType;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@RestController
public abstract class BaseController<T extends BaseEntity, U extends BaseDTO> {

    @Autowired
    protected JpaRepository<T, Long> repository ;

    @Autowired
    protected JpaSpecificationExecutor<T> specificationExecutor;

    @Autowired
    protected ModelMapper modelMapper;

    protected TypeMap<T, U> typeMap;		//for child

    private Class<T> entityClass;

    private Class<U> dtoClass;

    public BaseController() {

        this.entityClass = (Class<T>) ((ParameterizedType) getClass()
                .getGenericSuperclass()).getActualTypeArguments()[0];

        this.dtoClass = (Class<U>) ((ParameterizedType) getClass()
                .getGenericSuperclass()).getActualTypeArguments()[1];

    }

    @GetMapping
    public ResponseEntity<Page<?>> getAll(@RequestParam(value = "search", required = false) String search, Pageable pageable) {

        if (search != null) {
            GenericSpecificationBuilder<T> builder = new GenericSpecificationBuilder<>();

            String operationSetExper = Joiner.on("|").join(SearchOperation.SIMPLE_OPERATION_SET);
            Pattern pattern = Pattern.compile(
                    "(\\w+?)(" + operationSetExper + ")(\\p{Punct}?)(\\w+?)(\\p{Punct}?),");
            Matcher matcher = pattern.matcher(search + ",");
            while (matcher.find()) {
                builder.with(
                        matcher.group(1),
                        matcher.group(2),
                        matcher.group(4),
                        matcher.group(3),
                        matcher.group(5));
            }

            Specification<T> spec = builder.build();
            Page<T> data = specificationExecutor.findAll(spec, pageable); //spec here
            Page<U> dtoPage = convertToDtoPage(data);
            return ResponseEntity.ok(dtoPage);
        }else{
            Page<T> data = repository.findAll(pageable);
            Page<U> dtoPage = convertToDtoPage(data);
            return ResponseEntity.ok(dtoPage);
        }
    }
    @RequestMapping(value="/{id}", method = RequestMethod.GET)
    public ResponseEntity<?> getById(@PathVariable(value="id") Long id) {
        T entity = repository.findOne(id);
        if (entity == null) {
            throw new ObjectNotFoundException(null, null); //or as above
        }
        return new ResponseEntity<U>(this.entityToDto(entity), HttpStatus.OK);
    }


    @PostMapping
    public ResponseEntity<T> create(@RequestBody U dto) {
        T entity = this.dtoToEntity(dto);
        repository.save(entity);
        return new ResponseEntity<T>(entity, HttpStatus.CREATED);
    }

    @RequestMapping(value = "/{id}", method=RequestMethod.PUT)
    public ResponseEntity<?> update(@RequestBody U dto, @PathVariable(value="id") Long id){

        T entityDb = repository.findOne(id);
        if (entityDb == null){
            return new ResponseEntity("Entity not found", HttpStatus.NOT_FOUND);
        }
        dto.setId(id);
        T entity = this.dtoToEntity(dto);
        entity.setVersion(entityDb.getVersion()); //little bit stupid, but it's working!
        entity.setId(id);
        repository.save(entity);
        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }

    @RequestMapping(value = "/{id}", method=RequestMethod.DELETE)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete(@PathVariable(value="id") Long id){
        repository.delete(id);
    }


    protected Page<U> convertToDtoPage(Page<T> entityPage) {

        Page<U> dtoPage = entityPage.map(new Converter<T, U>() {
            @Override
            public U convert(T entity) {
                return entityToDto(entity);
            }
        });

        return dtoPage;
    }




    protected U entityToDto(T entity) {

        if (typeMap == null) {
            typeMap = modelMapper.createTypeMap(entity, this.dtoClass);
        }
        U dto = typeMap.map(entity);
        return dto;
    }

    protected T dtoToEntity(U dto) {
        T entity = modelMapper.map(dto, this.entityClass);
        return entity;
    }
}
