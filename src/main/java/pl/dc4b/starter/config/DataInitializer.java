package pl.dc4b.starter.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.security.authentication.encoding.ShaPasswordEncoder;
import org.springframework.stereotype.Component;
import pl.dc4b.starter.dao.EmployeeRepository;
import pl.dc4b.starter.dao.GenericRepository;
import pl.dc4b.starter.dao.RoleRepository;
import pl.dc4b.starter.dao.UserRepository;
import pl.dc4b.starter.entity.Employee;
import pl.dc4b.starter.entity.Grade;
import pl.dc4b.starter.entity.Role;
import pl.dc4b.starter.entity.User;
import pl.dc4b.starter.service.EmployeeService;
import pl.dc4b.starter.service.GenericService;
import pl.dc4b.starter.service.RoleService;
import pl.dc4b.starter.service.UserService;

import java.util.Arrays;

@Component
public class DataInitializer implements ApplicationRunner {

    @Value("${security.encoding-strength}")
    private Integer encodingStrength;

	@Autowired
    private UserRepository userRepository;
	
	@Autowired
    private RoleRepository roleRepository;

	@Autowired
    private EmployeeRepository employeeRepository;

	@Autowired
    private GenericRepository<User> genericUserRepository;

    @Autowired
    private GenericRepository<Role> genericRoleRepository;

    @Autowired
    private GenericService<Role> genericRoleService;

    @Autowired
    private GenericService<User> genericUserService;

    @Autowired
    private GenericService<Employee> genericEmployeeService;

    @Autowired
    private RoleService roleService;

    @Autowired
    private UserService userService;

    @Autowired
    private EmployeeService employeeService;

    public void run(ApplicationArguments args) {

    	Role role1 = new Role("ADMIN_USER", "Admin User - Has permission to perform admin tasks");
        roleRepository.save(role1); //generic repo

        Role role2 = new Role("STANDARD_USER", "Standard User - Has no admin rights");
        genericRoleService.getRepository().save(role2); //generic service

        Role role3 = new Role("GUEST", "Guest - can view only");
        roleService.getRepository().save(role3); //generic service with generic repo
        roleService.doSomethingWithRole(11L);

    	User u1 = new User("diwadu@gmail.com",
                new ShaPasswordEncoder(encodingStrength).encodePassword("test", null),
                "Dawid", "Chrzanowski", Arrays.asList(role1));
        genericUserService.create(u1);

        User u2 = new User("deska5@o2.pl",
                new ShaPasswordEncoder(encodingStrength).encodePassword("test", null),
                "Dawid", "Chrzanowski", Arrays.asList(role3));
        userService.create(u2);
        userService.doSomethingWithUser();

        Employee emp1 = new Employee("John", "Doe", Grade.JUNIOR, 1400);
        Employee emp2 = new Employee("Jakob", "Hogge", Grade.JUNIOR, 1500);
        Employee emp3 = new Employee("Arthur", "McKenzie", Grade.REGULAR, 3000);
        Employee emp4 = new Employee("Jane", "Doe", Grade.REGULAR, 3200);
        Employee emp5 = new Employee("Marta", "Spielberg", Grade.REGULAR, 3200);
        Employee emp6 = new Employee("Simon", "Smitch", Grade.SENIOR, 4000);
        Employee emp7 = new Employee("John", "Smitch", Grade.SENIOR, 4200);
        Employee emp8 = new Employee("Dawid", "Chrzanowski", Grade.ARCHITECT, 5000);

        employeeService.create(emp1);
        employeeService.create(emp2);
        employeeService.create(emp3);
        employeeService.create(emp4);
        employeeService.create(emp5);
        employeeService.create(emp6);
        employeeService.create(emp7);
        employeeService.create(emp8);
    }
}
