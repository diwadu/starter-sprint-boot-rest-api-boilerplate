package pl.dc4b.starter.entity;

public enum Grade {
    JUNIOR, REGULAR, SENIOR, ARCHITECT
}
