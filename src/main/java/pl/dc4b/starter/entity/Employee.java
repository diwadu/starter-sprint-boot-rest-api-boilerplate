package pl.dc4b.starter.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;


@Entity
@Table(name = "employee")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Employee extends BaseEntityAudit {
    private static final long serialVersionUID = 1L;

    @Column(name = "first_name")
    private String firstName;

    @Column(name = "last_name")
    private String lastName;

    @Enumerated //ordinal by default
    @Column(name="grade")
    //@Convert(converter = GradeConverter.class)
    private Grade grade;

    @Column(name="salary")
    private int salary;

}
