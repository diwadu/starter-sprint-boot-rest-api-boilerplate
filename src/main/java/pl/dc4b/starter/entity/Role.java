package pl.dc4b.starter.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Table(name="app_role")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Role extends BaseEntityAudit {

    private static final long serialVersionUID = 1L;

    @Column(name = "role_name")
    private String roleName;

    @Column(name = "description")
    private String description;

}
