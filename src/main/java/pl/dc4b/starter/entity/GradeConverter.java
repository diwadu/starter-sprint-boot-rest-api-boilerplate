package pl.dc4b.starter.entity;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;


/**
 * Unused
 */
@Converter
public class GradeConverter implements AttributeConverter<Grade, Integer> {

    @Override
    public Integer convertToDatabaseColumn(Grade grade) {
        return grade.ordinal();
    }

    @Override
    public Grade convertToEntityAttribute(Integer integer) {
        return Grade.values()[integer];
    }

}