package pl.dc4b.starter.service;

import pl.dc4b.starter.entity.User;

public interface UserService extends GenericService<User> {
    void doSomethingWithUser();
}
