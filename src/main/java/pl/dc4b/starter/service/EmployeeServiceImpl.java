package pl.dc4b.starter.service;

import org.springframework.stereotype.Service;
import pl.dc4b.starter.entity.Employee;

@Service
public class EmployeeServiceImpl extends GenericServiceImpl<Employee> implements EmployeeService {
}
