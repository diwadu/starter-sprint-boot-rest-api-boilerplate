package pl.dc4b.starter.service;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.dc4b.starter.entity.BaseEntity;

import java.util.List;

public interface GenericService<T extends BaseEntity> {
    JpaRepository<T, Long> getRepository();
    public List<T> findAll();
    public T findOne(Long id);
    public void create(T entity);
    public void update(T entity);
    public void update(Long id, T entity);
    public void delete(Long id);
    public void delete(T entity);
}
