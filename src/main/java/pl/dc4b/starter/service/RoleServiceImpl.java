package pl.dc4b.starter.service;

import org.springframework.stereotype.Service;
import pl.dc4b.starter.entity.Role;

@Service
public class RoleServiceImpl extends GenericServiceImpl<Role> implements RoleService {
    @Override
    public void doSomethingWithRole(Long id) {
        System.out.println("doSomething() called");
    }
}
