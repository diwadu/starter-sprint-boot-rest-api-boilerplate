package pl.dc4b.starter.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import pl.dc4b.starter.dao.GenericRepository;
import pl.dc4b.starter.entity.BaseEntity;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import java.util.List;

@Service
public abstract class GenericServiceImpl<T extends BaseEntity> implements GenericService<T> {

    @Autowired
    GenericRepository<T> genericRepository;

    @Override
    public JpaRepository<T, Long> getRepository() {
        return genericRepository;
    }


    @Override
    @Transactional(propagation = Propagation.REQUIRED, readOnly = true)
    public List<T> findAll() {
        return genericRepository.findAll();
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED, readOnly = true)
    public T findOne(Long id) {
        return genericRepository.findOne(id);
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public void create(T entity) {
        genericRepository.save(entity);
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public void update(T entity) {
        genericRepository.save(entity);
    }

    @Override
    public void update(Long id, T entity) {
        throw  new NotImplementedException();
    }

    @Override
    public void delete(Long id) {
        T entity = genericRepository.findOne(id);
        genericRepository.delete(entity);
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public void delete(T entity) {
        genericRepository.delete(entity);
    }
}
