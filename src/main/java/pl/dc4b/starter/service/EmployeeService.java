package pl.dc4b.starter.service;

import pl.dc4b.starter.entity.Employee;

public interface EmployeeService extends GenericService<Employee> {
}
