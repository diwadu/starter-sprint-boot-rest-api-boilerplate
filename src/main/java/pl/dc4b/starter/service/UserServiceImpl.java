package pl.dc4b.starter.service;

import org.springframework.stereotype.Service;
import pl.dc4b.starter.entity.User;

@Service
public class UserServiceImpl extends GenericServiceImpl<User> implements UserService {
    @Override
    public void doSomethingWithUser() {
        System.out.println("doSomethingWithUser() called");
    }
}
