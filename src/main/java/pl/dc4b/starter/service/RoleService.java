package pl.dc4b.starter.service;

import pl.dc4b.starter.entity.Role;

public interface RoleService extends GenericService<Role> {
    void doSomethingWithRole(Long id);
}
