### This is a SpringBoot based Rest API project for demonstration purposes. Project includes following concepts/libraries:

- Authentication/Authorization (based on JWT/OAuth2 and Roles [SpringSecurity]) - disabled in default
- DTO Mapping (Model Mapper)
- Jackson for JSON Serializaion 
- H2 Database
- Spring-data-JPA
- Lombok to eliminate getters and setters
- Guava for cleaner code
- Supports pagination, search, sorting

### Execution has been successfully tested with following tools
- jdk1.8.0_181
- IntellijIdea 2018.2.3 (Community Edition)
- IntellijIdea Lombok Plugin 0.27-2018.2
- Apache Maven 3.5.4

### How to execute
- Download or clone project
- Unpack
- Open in IntelijIdea (Lombok plugin must be installed)
- Build
- Run

##### No need to worry about initial data in database. Each time application is started, database is seeded automatically with startup set of data.


### API Endpoints:

##### No need to specify `content-type` header - all data will be served in JSON format

Entity example (paged) `http://localhost:8080/api/employees`:

```json
{
    "content": [
        {
            "id": 8,
            "firstName": "Dawid",
            "lastName": "Chrzanowski",
            "grade": "ARCHITECT",
            "salary": 5000
        }
    ],
    "totalElements": 8,
    "totalPages": 8,
    "last": true,
    "size": 1,
    "number": 7,
    "sort": null,
    "first": false,
    "numberOfElements": 1
}
```

#### List of employees
`http://localhost:8080/api/employees`

#### Employee 1 details
`http://localhost:8080/api/employees/1`

#### List of employees with salay bigger than 3000 
`http://localhost:8080/api/employees?search=salary>3000`

#### List of employees with firstName starts with "John"
`http://localhost:8080/api/employees?search=firstName:John*`

#### List of employees sorted by id (desc)
`http://localhost:8080/api/employees?sort=id,desc`

#### List of employees paged (page_size=2, page_nr= 3)
`http://localhost:8080/api/employees?page=3&size=2`

#### List of employees with firstName starts with "John", and salary between 3000 and 4300, results sorted by id in descending order, taken first 10 records page.
`http://localhost:8080/api/employees?search=firstName:John*,salary>3000,salary<4300&sort=id,desc&page=0&size=10`

#### In default CRUD operations are supported C-POST, R-GET, U-PUT, D-DELETE

#### If you want add new entity, all what need to be done is just create basic stuff:
- entity model derived from BaseEntity
- repository interface derived from GenericRepository
- service interface derived from GenericService interface
- serviceImpl class derived from GenericServiceImpl class
- controller derived from BaseController class
- entity DTO class derived from BaseDto class (required for controller)
- if required, initialize database with data in DataInitializer class 

### What next?
- update Java/Spring/Other Libraries
- use Swagger
- CQRS, reading based on db views (?), simplier domain, less complication with DTO classes
- divide in separate modules [apps?] (Models, DAO, SearchEngine, Mapping, WebBasicStufff, Configuration, others)

### Limitations
- no support in query string for filtering using enumerations (for example "Grade" field for "Employee" model) (or I don't know yet how to do it :) )
